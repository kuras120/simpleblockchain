use libp2p::floodsub::{Floodsub, FloodsubEvent, Topic};
use libp2p::{NetworkBehaviour, PeerId};
use libp2p::identity::Keypair;
use libp2p::mdns::{Mdns, MdnsEvent};
use libp2p::ping::{Ping, PingConfig, PingEvent};
use once_cell::sync::Lazy;
use serde::{Deserialize, Serialize};
use tokio::sync::mpsc::UnboundedSender;
use crate::modules::block::Block;
use crate::modules::blockchain::Blockchain;

pub static KEYS: Lazy<Keypair> = Lazy::new(Keypair::generate_ed25519);
pub static PEER_ID: Lazy<PeerId> = Lazy::new(|| PeerId::from(KEYS.public()));
pub static CHAIN_TOPIC: Lazy<Topic> = Lazy::new(|| Topic::new("chains"));
pub static BLOCK_TOPIC: Lazy<Topic> = Lazy::new(|| Topic::new("blocks"));

#[derive(Debug, Serialize, Deserialize)]
pub struct ChainResponse {
    pub blocks: Vec<Block>,
    pub receiver: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct LocalChainRequest {
    pub from_peer_id: String,
}

pub enum EventType {
    LocalChainResponse(ChainResponse),
    Input(String),
    Init,
}

#[derive(Debug)]
pub enum BlockchainEvent {
    Floodsub(FloodsubEvent),
    Mdns(MdnsEvent),
    Ping(PingEvent)
}

#[derive(NetworkBehaviour)]
#[behaviour(out_event="BlockchainEvent")]
pub struct BlockchainBehaviour {
    pub flood_sub: Floodsub,
    pub mdns: Mdns,
    pub ping: Ping,
    #[behaviour(ignore)]
    pub response_sender: UnboundedSender<ChainResponse>,
    #[behaviour(ignore)]
    pub init_sender: UnboundedSender<bool>,
    #[behaviour(ignore)]
    pub blockchain: Blockchain,
}

impl From<FloodsubEvent> for BlockchainEvent {
    fn from(value: FloodsubEvent) -> Self {
        BlockchainEvent::Floodsub(value)
    }
}

impl From<MdnsEvent> for BlockchainEvent {
    fn from(value: MdnsEvent) -> Self {
        BlockchainEvent::Mdns(value)
    }
}

impl From<PingEvent> for BlockchainEvent {
    fn from(value: PingEvent) -> Self {
        BlockchainEvent::Ping(value)
    }
}

impl BlockchainBehaviour {
    pub async fn new(
        blockchain: Blockchain,
        response_sender: UnboundedSender<ChainResponse>,
        init_sender: UnboundedSender<bool>
    ) -> Self {
        let mut behaviour = Self {
            blockchain,
            flood_sub: Floodsub::new(*PEER_ID),
            mdns: Mdns::new(Default::default()).await.expect("Can create mdns"),
            ping: Ping::new(PingConfig::new().with_keep_alive(true)),
            response_sender,
            init_sender
        };
        behaviour.flood_sub.subscribe(CHAIN_TOPIC.clone());
        behaviour.flood_sub.subscribe(BLOCK_TOPIC.clone());

        behaviour
    }
}
