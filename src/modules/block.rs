use std::time::{Duration, SystemTime, UNIX_EPOCH};
use serde::{Deserialize, Serialize};
use sha2::{Digest, Sha256};
use crate::modules::utils;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Block {
    pub id: u64,
    pub prev_hash: String,
    pub timestamp: u128,
    pub data: String,
    pub nonce: Option<u64>,
    pub hash: Option<String>,
}

impl Block {
    pub fn new(id: u64, prev_hash: String, data: String) -> Self {
        let timestamp = SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .unwrap_or(Duration::from_millis(0))
            .as_millis();
        Self {
            id,
            prev_hash,
            timestamp,
            data,
            nonce: None,
            hash: None
        }
    }

    pub fn mine(&mut self) -> (u64, String) {
        info!("Mining block...");
        let mut nonce = 0;
        loop {
            if nonce % 100000 == 0 {
                info!("Nonce: {}", nonce)
            }
            let hash = self.calculate_hash(nonce);
            let binary_hash = utils::hash_to_binary(&hash);
            if binary_hash.starts_with(utils::DIFFICULTY_PREFIX) {
                self.nonce = Some(nonce);
                let hexed_hash = hex::encode(hash);
                self.hash = Some(hexed_hash.clone());
                info!("Mined! Nonce: {}, hash: {:?}", nonce, hexed_hash);
                return (nonce, hexed_hash);
            }
            nonce += 1;
        }
    }

    pub fn calculate_hash(&self, nonce: u64) -> Vec<u8> {
        let data = serde_json::json!({
            "id": self.id,
            "prev_hash": self.prev_hash,
            "data": self.data,
            "timestamp": self.timestamp
        });
        let mut hasher = Sha256::new();
        hasher.update(data.to_string().as_bytes());
        hasher.update(nonce.to_le_bytes());
        hasher.finalize().as_slice().to_owned()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_mines_block() {
        let mut block = Block::new(1, String::from("GENESIS"), String::from("DATA"));
        let (nonce, hex_hash) = block.mine();
        assert_eq!(nonce, block.nonce.unwrap_or(0));
        let hash = block.calculate_hash(block.nonce.expect("Hash exists"));
        assert_eq!(hex_hash, hex::encode(hash));
    }
}
