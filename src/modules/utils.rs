pub const DIFFICULTY_PREFIX: &str = "00";

pub fn hash_to_binary(hash: &Vec<u8>) -> String {
    let mut res: String = String::default();
    for c in hash {
        res.push_str(&format!("{:b}", c));
    }
    return res;
}
