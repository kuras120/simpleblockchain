use std::collections::HashSet;
use libp2p::floodsub::FloodsubMessage;
use libp2p::Swarm;
use crate::modules::block::Block;
use crate::modules::p2p::{BLOCK_TOPIC, BlockchainBehaviour, CHAIN_TOPIC, ChainResponse, LocalChainRequest, PEER_ID};

pub fn get_list_peers(swarm: &Swarm<BlockchainBehaviour>) -> Vec<String> {
    let nodes = swarm.behaviour().mdns.discovered_nodes();
    let mut unique_peers = HashSet::new();
    for peer in nodes {
        unique_peers.insert(peer);
    }
    unique_peers.iter().map(|p| p.to_string()).collect()
}

pub fn handle_print_peers(swarm: &Swarm<BlockchainBehaviour>) {
    info!("Discovered Peers:");
    let peers = get_list_peers(swarm);
    peers.iter().for_each(|p| info!("{}", p));
}

pub fn handle_print_chain(swarm: &Swarm<BlockchainBehaviour>) {
    info!("Local Blockchain:");
    let pretty_json =
        serde_json::to_string_pretty(&swarm.behaviour().blockchain.blocks).expect("can jsonify blocks");
    info!("{}", pretty_json);
}

pub fn handle_create_block(data: Option<&str>, swarm: &mut Swarm<BlockchainBehaviour>) {
    if let Some(data) = data {
        let behaviour = swarm.behaviour_mut();
        let latest_block = behaviour
            .blockchain
            .blocks
            .last()
            .expect("There is at least one block");
        let mut block = Block::new(
            latest_block.id + 1,
            latest_block.hash.clone().expect("Hash exists"),
            data.trim().to_owned()
        );
        block.mine();
        let json = serde_json::to_string(&block).expect("Can jsonify request");
        info!("Broadcasting new block: {:?}", &block);
        behaviour.blockchain.blocks.push(block);
        behaviour.flood_sub.publish(BLOCK_TOPIC.clone(), json.as_bytes());
    }
}

pub fn handle_request_network_chain(swarm: &mut Swarm<BlockchainBehaviour>, with_init: Option<bool>) {
    let peers = get_list_peers(&swarm);

    if with_init.unwrap_or(true) {
        swarm.behaviour_mut().blockchain.init();
    }

    info!("Connected nodes: {}", peers.len());
    if !peers.is_empty() {
        let req = LocalChainRequest {
            from_peer_id: peers.iter().last().expect("At least one peer").to_string(),
        };

        let json = serde_json::to_string(&req).expect("Can jsonify request");
        swarm.behaviour_mut().flood_sub.publish(CHAIN_TOPIC.clone(), json.as_bytes());
    }
}

pub fn handle_receive_network_chain(resp: ChainResponse, msg: FloodsubMessage, blockchain_behaviour: &mut BlockchainBehaviour) {
    if resp.receiver == PEER_ID.to_string() {
        info!("Getting chain from network: {}", msg.source);
        resp.blocks.iter().for_each(|r| info!("{:?}", r));
        blockchain_behaviour.blockchain.blocks = blockchain_behaviour.blockchain.choose_chain(
            blockchain_behaviour.blockchain.blocks.clone(), resp.blocks
        )
    }
}

pub fn handle_send_local_network_chain(resp: LocalChainRequest, msg: FloodsubMessage, blockchain_behaviour: &mut BlockchainBehaviour) {
    info!("Sending local chain to {}", msg.source.to_string());
    let peer_id = resp.from_peer_id;
    if PEER_ID.to_string() == peer_id {
        if let Err(e) = blockchain_behaviour.response_sender.send(ChainResponse {
            blocks: blockchain_behaviour.blockchain.blocks.clone(),
            receiver: msg.source.to_string(),
        }) {
            error!("Error sending response via channel, {}", e)
        }
    }
}

pub fn handle_receive_block(block: Block, msg: FloodsubMessage, blockchain_behaviour: &mut BlockchainBehaviour) {
    info!("Received new block from {}", msg.source.to_string());
    blockchain_behaviour.blockchain.add_block(block);
}
