use crate::modules::block::Block;
use std::time::{Duration, SystemTime, UNIX_EPOCH};
use crate::modules::utils::{DIFFICULTY_PREFIX, hash_to_binary};

#[derive(Debug)]
pub struct Blockchain {
    pub blocks: Vec<Block>,
}

impl Blockchain {
    pub fn new() -> Self {
        Self { blocks: vec![] }
    }

    pub fn init(&mut self) -> &Self {
        let mut genesis_block = Block {
            id: 0,
            prev_hash: String::from("GENESIS"),
            timestamp: SystemTime::now()
                .duration_since(UNIX_EPOCH)
                .unwrap_or(Duration::from_millis(0))
                .as_millis(),
            data: String::from("GENESIS"),
            nonce: None,
            hash: None,
        };
        genesis_block.mine();
        info!("Genesis block created!");
        self.blocks.push(genesis_block);
        self
    }

    pub fn add_block(&mut self, block: Block) {
        let latest_block = self.blocks.last().expect("Block list should not be empty");
        if self.is_block_valid(&block, latest_block) {
            self.blocks.push(block);
        } else {
            error!("Could not add block");
        }
    }

    pub fn is_block_valid(&self, current_block: &Block, latest_block: &Block) -> bool {
        if !current_block.prev_hash.eq(latest_block.hash.as_ref().expect("Hash should exists")) {
            warn!("Block with id: {} has wrong prev hash", current_block.id);
            return false;
        } else if !hash_to_binary(&hex::decode(&current_block.hash.as_ref().expect("Hash should exists"))
            .expect("Cannot decode from hex")).starts_with(DIFFICULTY_PREFIX) {
            warn!("Block with id: {} has invalid difficulty", current_block.id);
            return false;
        } else if current_block.id != latest_block.id + 1 {
            warn!("Block with id: {} is not the next block after the latest: {}", current_block.id, latest_block.id);
            return false;
        } else if !hex::encode(current_block.calculate_hash(current_block.nonce.expect("Nonce should exists")))
            .eq(current_block.hash.as_ref().expect("Hash should exists")) {
            warn!("Block with id: {} has invalid hash", current_block.id);
            return false;
        }
        true
    }

    pub fn is_chain_valid(&self, chain: &[Block]) -> bool {
        for i in 0..chain.len() {
            if i == 0 {
                continue;
            }
            let first = chain.get(i - 1).expect("has to exist");
            let second = chain.get(i).expect("has to exist");
            if !self.is_block_valid(second, first) {
                return false;
            }
        }
        true
    }

    pub fn choose_chain(&mut self, local: Vec<Block>, remote: Vec<Block>) -> Vec<Block> {
        let is_local_valid = self.is_chain_valid(&local);
        let is_remote_valid = self.is_chain_valid(&remote);

        if is_local_valid && is_remote_valid {
            if local.len() == 1 { remote } else {
                if local.len() >= remote.len() { local } else { remote }
            }
        } else if is_remote_valid && !is_local_valid {
            remote
        } else if !is_remote_valid && is_local_valid {
            local
        } else {
            panic!("Local and remote chains are both invalid");
        }
    }
}
