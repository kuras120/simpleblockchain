use std::time::Duration;
use libp2p::core::upgrade;
use libp2p::{mplex, Swarm, Transport};
use libp2p::floodsub::FloodsubEvent;
use libp2p::futures::StreamExt;
use libp2p::mdns::MdnsEvent;
use libp2p::noise::{Keypair, NoiseConfig, X25519Spec};
use libp2p::swarm::{SwarmBuilder, SwarmEvent};
use libp2p::tcp::{TokioTcpConfig};
use log::{debug, error, info};
use tokio::io::{AsyncBufReadExt, BufReader, stdin};
use tokio::{select, spawn};
use tokio::sync::mpsc;
use tokio::time::sleep;
use simpleblockchain::modules::blockchain::Blockchain;
use simpleblockchain::modules::{p2p, p2p_helper};
use simpleblockchain::modules::block::Block;
use simpleblockchain::modules::p2p::{BlockchainEvent, ChainResponse, LocalChainRequest, PEER_ID};
use simpleblockchain::modules::p2p_helper::{handle_receive_network_chain, handle_receive_block, handle_send_local_network_chain};

#[tokio::main]
async fn main() {
    pretty_env_logger::init();

    info!("Peer id: {}", PEER_ID.clone());
    let (response_sender,
        mut response_rcv) = mpsc::unbounded_channel();
    let (init_sender,
        mut init_rcv) = mpsc::unbounded_channel();

    let auth_keys =
        Keypair::<X25519Spec>::new().into_authentic(&p2p::KEYS).expect("Can create auth keys");

    let transport =
        TokioTcpConfig::new()
            .upgrade(upgrade::Version::V1)
            .authenticate(NoiseConfig::xx(auth_keys).into_authenticated())
            .multiplex(mplex::MplexConfig::new())
            .boxed();

    let behaviour =
        p2p::BlockchainBehaviour::new(Blockchain::new(), response_sender, init_sender.clone()).await;

    let mut swarm =
        SwarmBuilder::new(transport, behaviour, *PEER_ID).executor(Box::new(|fut| {
            spawn(fut);
        })).build();

    let mut stdin = BufReader::new(stdin()).lines();

    Swarm::listen_on(
        &mut swarm,
        "/ip4/0.0.0.0/tcp/0"
            .parse()
            .expect("Can get a local socket"),
    ).expect("Swarm can be started");

    spawn(async move {
        sleep(Duration::from_secs(1)).await;
        info!("Sending init event");
        init_sender.send(true).expect("Can send init event");
    });

    loop {
        let event = select! {
            line = stdin.next_line() => Some(
                p2p::EventType::Input(line.expect("Can get line").expect("Can read line from stdin"))
            ),
            response = response_rcv.recv() => Some(
                p2p::EventType::LocalChainResponse(response.expect("Response exists"))
            ),
            _init = init_rcv.recv() => Some(
                p2p::EventType::Init
            ),
            event = swarm.select_next_some() => {
                if let SwarmEvent::Behaviour(blockchain_event) = event {
                    match blockchain_event {
                        BlockchainEvent::Floodsub(floodsub_event) => {
                            if let FloodsubEvent::Message(msg) = floodsub_event {
                                if let Ok(resp) = serde_json::from_slice::<ChainResponse>(&msg.data) {
                                    handle_receive_network_chain(resp, msg, swarm.behaviour_mut());
                                } else if let Ok(resp) = serde_json::from_slice::<LocalChainRequest>(&msg.data) {
                                    handle_send_local_network_chain(resp, msg, swarm.behaviour_mut());
                                } else if let Ok(block) = serde_json::from_slice::<Block>(&msg.data) {
                                    handle_receive_block(block, msg, swarm.behaviour_mut());
                                }
                            }
                        },
                        BlockchainEvent::Mdns(mdns_event) => {
                            match mdns_event {
                                MdnsEvent::Discovered(discovered_list) => {
                                    for (peer, _addr) in discovered_list {
                                        swarm.behaviour_mut().flood_sub.add_node_to_partial_view(peer);
                                    }
                                },
                                MdnsEvent::Expired(expired_list) => {
                                    let blockchain_behaviour = swarm.behaviour_mut();
                                    for (peer, _addr) in expired_list {
                                        if !blockchain_behaviour.mdns.has_node(&peer) {
                                            blockchain_behaviour.flood_sub.remove_node_from_partial_view(&peer);
                                        }
                                    }
                                }
                            }
                        },
                        BlockchainEvent::Ping(ping_event) => {
                            debug!("Ping {:?}", ping_event)
                        }
                    }
                    None
                } else {
                    debug!("Unhandled Swarm Event {:?}", event);
                    None
                }
            },
        };

        if let Some(event) = event {
            match event {
                p2p::EventType::Init => {
                    p2p_helper::handle_request_network_chain(&mut swarm, Some(true));
                }
                p2p::EventType::LocalChainResponse(resp) => {
                    let json = serde_json::to_string(&resp).expect("Can jsonify response");
                    swarm.behaviour_mut().flood_sub.publish(p2p::CHAIN_TOPIC.clone(), json.as_bytes());
                }
                p2p::EventType::Input(line) => match line.as_str() {
                    "ls peer" => p2p_helper::handle_print_peers(&swarm),
                    "ls chain" => p2p_helper::handle_print_chain(&swarm),
                    "recreate chain" => p2p_helper::handle_request_network_chain(&mut swarm, Some(false)),
                    cmd if cmd.starts_with("create block") => p2p_helper::handle_create_block(cmd.strip_prefix("create block"), &mut swarm),
                    _ => error!("Unknown command"),
                }
            }
        }
    }
}
